﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class StartActivator : MonoBehaviour
{
    [SerializeField] TMP_InputField playerOneInput;
    [SerializeField] TMP_InputField playerTwoInput;
    [SerializeField] Button start;

    public GameStatus gameStatus;

    private void Start()
    {
        start.interactable = false;
    }

    // Update is called once per frame
    void Update()
    {
        PlayersNamesStatus();
    }

    private void PlayersNamesStatus()
    {
        if (playerOneInput.text != "" && playerTwoInput.text != "")
        {
            start.interactable = true;
        }
            
        else start.interactable = false;
    }

    public void PassOnPlayersNames()
    {
        gameStatus.GetPlayersNames(playerOneInput, playerTwoInput);
    }
}
