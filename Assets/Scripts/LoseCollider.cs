﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class LoseCollider : MonoBehaviour
{
    //config params
    [SerializeField] [Range(1,2)] int playerSide;
    [SerializeField] TextMeshProUGUI oponentPlayerName;

    // cache references
    public GameStatus gameStatus;
    AudioSource loosingSound;

    private void Start()
    {
        loosingSound = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (gameStatus.GetLives(playerSide) > 0)
            {
            loosingSound.Play();
            FindObjectOfType<GameStatus>().UpdateScore(playerSide);
            FindObjectOfType<Ball>().hasStarted = false;   
            Debug.Log(gameStatus.GetLives(playerSide));
            }
        else
        {
          //  winner() pass on Oponent's value;
            SceneManager.LoadScene("WinMenu Screen");
        }
    }
}

