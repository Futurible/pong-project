﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameStatus : MonoBehaviour {

    // Configuration params
    [Range(0.1f, 10f)] [SerializeField] float gameSpeed = 1f;
    [SerializeField] TextMeshProUGUI player1Lives;
    [SerializeField] TextMeshProUGUI player2Lives;
    [SerializeField] TextMeshProUGUI player1Name;
    [SerializeField] TextMeshProUGUI player2Name;

    // Cache references
    private int player1LivesCounter = 3;
    private int player2LivesCounter = 3;

    private void Awake()
    {
        int gameStatusCount = FindObjectsOfType<GameStatus>().Length;
        if (gameStatusCount > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        player1Lives.text = player1LivesCounter.ToString();
        player2Lives.text = player2LivesCounter.ToString();
    }

    // Update is called once per frame
    void Update () {
        Time.timeScale = gameSpeed;
	}

    public void UpdateScore(int player)
    {
        switch (player)
        {
            case 1:
                player1LivesCounter -= 1;
                player1Lives.text = player1LivesCounter.ToString();
                break;
            case 2:
                player2LivesCounter -= 1;
                player2Lives.text = player2LivesCounter.ToString();
                break;
        } 
    }

    public int GetLives(int player)
    {
        if (player == 1)
        {
            return player1LivesCounter;
        }
        else
            return player2LivesCounter;  
    }

    public void GetPlayersNames (TMP_InputField one, TMP_InputField two)
    {
        player1Name.text = one.text;
        player2Name.text = two.text;
    }

    public void ResetSession()
    {
        Destroy(gameObject);
    }

}
