﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour {

    // Params
    [SerializeField] float speed = 10f;
    [SerializeField] KeyCode moveDown;
    [SerializeField] KeyCode moveUp;

    // Cache references
    float minPaddlePosY = -3.8f;
    float maxPaddlePosY = 3.8f;
    float thePaddle;

    // Use this for initialization
    void Start () {

        thePaddle = GetComponent<Rigidbody2D>().velocity.y;
    }
	
	// Update is called once per frame
	void Update () {

        PaddleMovement(); 
    }

    private void PaddleMovement()
    {
        Vector2 paddlePos = new Vector2(transform.position.x, transform.position.y);
        paddlePos.y = Mathf.Clamp(transform.position.y, minPaddlePosY, maxPaddlePosY);
        transform.position = paddlePos;

        if (Input.GetKey(this.moveDown))
        {
            transform.position += Vector3.down * speed * Time.deltaTime;
        }
        if (Input.GetKey(this.moveUp))
        {
            transform.position += Vector3.up * speed * Time.deltaTime;
        }
    }
}
